Source: django-model-utils
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Brian May <bam@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all:any,
 python3-django,
 python3-freezegun,
# python3-pytest,
# python3-pytest-django,
 python3-setuptools,
 python3-sphinx,
 python3-sphinx-rtd-theme,
Rules-Requires-Root: no
Standards-Version: 4.6.1
Homepage: https://github.com/jazzband/django-model-utils
Vcs-Git: https://salsa.debian.org/python-team/packages/django-model-utils.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/django-model-utils

Package: python-django-model-utils-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends}
Multi-Arch: foreign
Description: Django model mixins and utilities — Documentation
 Django is a high-level web application framework that loosely follows
 the model-view-controller design pattern.
 .
 The ‘django-model-utils’ library provides some mixins and utilities
 for Django:
 .
  * QueryManager: one-line definition of Managers returning a
    custom QuerySet.
  * InheritanceCastModel: more efficient use of model inheritance
  * TimeStampedModel: self-updating ‘created’ and ‘modified’ fields.
 .
 This package installs the documentation for the library.

Package: python3-django-model-utils
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Suggests: python-django-model-utils-doc
Description: Django model mixins and utilities — Python 3
 Django is a high-level web application framework that loosely follows
 the model-view-controller design pattern.
 .
 The ‘django-model-utils’ library provides some mixins and utilities
 for Django:
 .
  * QueryManager: one-line definition of Managers returning a
    custom QuerySet.
  * InheritanceCastModel: more efficient use of model inheritance
  * TimeStampedModel: self-updating ‘created’ and ‘modified’ fields.
 .
 This package installs the library for Python 3.
